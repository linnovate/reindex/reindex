version: "2.1"

services:
# Nginx
  nginx:
    image: nginx:latest
    container_name: ${PROJECT_NAME}_nginx
    volumes:
      - "./wordpress:/var/www/html"
      - "dist:/var/www/html/wp-content/plugins/reindex-widgets/assets/client-render"
      - ./nginx/config/local.nginx.conf:/etc/nginx/conf.d/default.conf
    ports:
      - "${NGINX_PORT}:80"
    networks:
      - reindex
    depends_on:
      - wordpress

# Wordpress + React
  wordpress:
    image: wordpress:5.3.2-php7.2-fpm
    container_name: ${PROJECT_NAME}_wordpress
    environment:
      WORDPRESS_DB_HOST: "mysql:${MYSQL_PORT}"
      WORDPRESS_DB_USER: "${MYSQL_USER}"
      WORDPRESS_DB_PASSWORD: "${MYSQL_PASSWORD}"
      WORDPRESS_DB_NAME: "${WORDPRESS_DB_NAME}"
    volumes:
      - "./wordpress:/var/www/html"
    depends_on:
      - mysql
    networks:
      - reindex

  ui:
    build:
      context: ./ui
      dockerfile: Dockerfile-dev
    working_dir: /usr/src/app
    container_name: ${PROJECT_NAME}_ui
    volumes:
      - ./ui:/usr/src/app
      - "/usr/src/app/node_modules"
      - dist:/usr/src/app/dist
    environment:
      PORT: "${UI_PORT}"
      NODE_ENV: ${NODE_ENV}
      WORKER: ${UI_SERVICE_WORKER}
      GRAPH_URL: "http://localhost:${NGINX_PORT}/graphql"
      PUBLIC_PATH: "http://localhost:${NGINX_PORT}/wp-content/plugins/reindex-widgets/assets/client-render/"
    ports:
      - "${UI_PORT}:${UI_PORT}"
    networks:
      - reindex
    command: npm run dev


# Reindex API
  graph:
    build:
      context: ./reindex/graph
      dockerfile: Dockerfile-dev
    working_dir: /usr/src/app
    container_name: ${PROJECT_NAME}_graph
    volumes:
      - ./reindex/graph/src:/usr/src/app/src
      - "./modules/plugins:/usr/src/app/node_modules/@reindex/plugins"
      - "./modules/plugins:/usr/src/app/plugins"
    environment:
      PORT: "${GRAPH_PORT}"
      NODE_ENV: development
      DEBUG: 3
      ALERT: 1
      AUTHENTICATION_COOKIE_NAME: "reindex_auth"
      ELASTIC_SEARCH_URL: "http://elasticsearch:${ELASTICSEARCH_PORT}/"
      ELASTIC_SEARCH_INDEX: ${ELASTICSEARCH_INDEX}
      PROFILE_URI: "http://profile:${PROFILE_API_PORT}/"
      MODEL_URI: "http://model:${MEDICAL_REPORT_API_PORT}/"
      GROUP_URI: "http://group:${GROUP_API_PORT}/"
      CONSUMER_URI: "http://consumer:${CONSUMER_PORT}/"
      NOTIFICATIONS_URI: "http://notifications:${NOTIFICATIONS_PORT}/"
      WORDPRESS_URI: "http://nginx/wp-json/reindex_wp_auth/"
      RABBIT_MQ_URL: "amqp://rabbitmq:${RABBITMQ_AMQP_PORT}"
      APOLLO_ENGINE_API_KEY: "service:experts:S3AyxT6IhLf8EBQuOkflZw"
      REDIS_HOST: redis
      REDIS_PORT: "${REDIS_PORT}"
      REDIS_PASSWORD: ""
      REDIS_LOG: 1
      REDIS_EXPIRATION_TIME: 60
      USE_REDIS: 1
      ADMIN_PASSWORD: "adminPassword"
      ADMIN_SECRET_TOKEN: "verysecret"
      ALERT_MANAGER_URL: ""
      ALERT_MANAGER_USER: ""
      ALERT_MANAGER_PASSWORD: ""
      DOCKERFILE_BUILD_PROD_SCRIPT: "build:production"
      SERVICE_NAME: "graph"
      ROLES: "user admin"
    ports:
      - "${GRAPH_PORT}:${GRAPH_PORT}"
    networks:
      - reindex

  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:6.6.0
    container_name: ${PROJECT_NAME}_elasticsearch
    volumes:
      - esdata:/usr/share/elasticsearch/data
    ports:
      - "${ELASTICSEARCH_PORT}:${ELASTICSEARCH_PORT}"
    environment:
      - "ES_JAVA_OPTS=-Xms512m -Xmx1G"
      - "discovery.type=single-node"
    networks:
      - reindex

  elastichq:
    container_name: ${PROJECT_NAME}_elastichq
    image: polinux/elastichq:latest
    environment:
      - "HQ_DEFAULT_URL=http://elasticsearch:${ELASTICSEARCH_PORT}"
    ports:
      - "${ELASTICSEARCH_HQ_PORT}:${ELASTICSEARCH_HQ_PORT}"
    networks:
      - reindex

  redis:
    image: redis
    container_name: ${PROJECT_NAME}_redis
    volumes:
      - ./data/redis:/data
    command: ["redis-server", "--bind", "redis", "--port", "${REDIS_PORT}"]
    ports:
      - "${REDIS_PORT}:${REDIS_PORT}"
    networks:
      - reindex

  consumer:
    build:
      context: ./reindex/consumer
      dockerfile: Dockerfile-dev
    working_dir: /usr/src/app
    container_name: ${PROJECT_NAME}_consumer
    volumes:
      - ./reindex/consumer/src:/usr/src/app/src
      - "./modules/plugins:/usr/src/app/node_modules/@reindex/plugins"
      - "./modules/plugins:/usr/src/app/plugins"
    environment:
      RABBITMQ_URI: "amqp://reindex:newstart@rabbitmq:${RABBITMQ_AMQP_PORT}"
      ELASTIC_SEARCH_URL: "elasticsearch:${ELASTICSEARCH_PORT}"
      ELASTIC_SEARCH_INDEX: ${ELASTICSEARCH_INDEX}
      NODE_ENV: development
      PORT: "${CONSUMER_PORT}"
      DEBUG: 3
      REDIS_HOST: redis
      REDIS_PORT: "${REDIS_PORT}"
      REDIS_PASSWORD: ""
      REDIS_LOG: 1
      REDIS_EXPIRATION_TIME: 60
      USE_REDIS: 1
      ALERT_MANAGER_URL: ""
      ALERT_MANAGER_USER: ""
      ALERT_MANAGER_PASSWORD: ""
      DOCKERFILE_BUILD_PROD_SCRIPT: start
      SERVICE_NAME: "consumer"
    depends_on:
      - rabbitmq
    ports:
      - "${CONSUMER_PORT}:${CONSUMER_PORT}"
    networks:
      - reindex

  notifications:
    build:
      context: ./reindex/consumer
      dockerfile: Dockerfile-dev
    working_dir: /usr/src/app
    container_name: ${PROJECT_NAME}_notifications
    volumes:
      - ./reindex/consumer/src:/usr/src/app/src
      - "./modules/plugins:/usr/src/app/node_modules/@reindex/plugins"
      - "./modules/plugins:/usr/src/app/plugins"
    environment:
      RABBITMQ_URI: "amqp://reindex:newstart@rabbitmq:${RABBITMQ_AMQP_PORT}"
      NODE_ENV: development
      PORT: "${NOTIFICATIONS_PORT}"
      DEBUG: 3
      REDIS_HOST: redis
      REDIS_PORT: "${REDIS_PORT}"
      REDIS_PASSWORD: ""
      REDIS_LOG: 1
      REDIS_EXPIRATION_TIME: 60
      USE_REDIS: 1
      ALERT_MANAGER_URL: ""
      ALERT_MANAGER_USER: ""
      ALERT_MANAGER_PASSWORD: ""
      DOCKERFILE_BUILD_PROD_SCRIPT: start
      SERVICE_NAME: "notifications"
      TWILIO_ACCOUNT_SID: ACd751971117e13270a95cf6f93ed50a78
      TWILIO_API_KEY: SK442ef24389324e234eb4dd0bd021336c
      TWILIO_API_SECRET: fkcbN9tLIScuKejxGzco0LTvBId8qyO8
      TWILIO_NOTIFICATION_SERVICE_SID: ISa3644386b367204f87e9f43b5d3c86d2
    depends_on:
      - rabbitmq
    ports:
      - "${NOTIFICATIONS_PORT}:${NOTIFICATIONS_PORT}"
    networks:
      - reindex

  rabbitmq:
    image: rabbitmq:3-management
    container_name: ${PROJECT_NAME}_rabbit
    environment:
      RABBITMQ_DEFAULT_USER: "reindex"
      RABBITMQ_DEFAULT_PASS: "newstart"
    volumes:
      - ./reindex/.rabbitmq_enabled_plugins:/etc/rabbitmq/enabled_plugins
    ports:
      - "${RABBITMQ_MANAGEMENT_PORT}:${RABBITMQ_MANAGEMENT_PORT}"
      - "${RABBITMQ_MQTT_PORT}:${RABBITMQ_MQTT_PORT}"
      - "${RABBITMQ_AMQP_PORT}:${RABBITMQ_AMQP_PORT}"
    networks:
      - reindex

  model:
    build:
      context: ./reindex/model
      dockerfile: Dockerfile-dev
    working_dir: /usr/src/app
    container_name: ${PROJECT_NAME}_model
    volumes:
      - ./reindex/model/src:/usr/src/app/src
      - "./modules/plugins:/usr/src/app/node_modules/@reindex/plugins"
      - "./modules/plugins:/usr/src/app/plugins"
    environment:
      MONGO_DB_URI: "mongodb://mongo:${MONGO_PORT}/${MONGO_DB_NAME}"
      MONGO_LOST_CONNECTION_RETRIES_ALERT: 5
      ELASTIC_SEARCH_URL: "elasticsearch:${ELASTICSEARCH_PORT}"
      PORT: "${MEDICAL_REPORT_API_PORT}"
      DEBUG: 3
      NODE_ENV: development
      SESSION_SECRET: ""
      ADMIN_SECRET_TOKEN: "verysecret"
      ALERT_MANAGER_URL: ""
      ALERT_MANAGER_USER: ""
      ALERT_MANAGER_PASSWORD: ""
      DOCKERFILE_BUILD_PROD_SCRIPT: start
      SERVICE_NAME: "model"
    ports:
      - "${MEDICAL_REPORT_API_PORT}:${MEDICAL_REPORT_API_PORT}"
    depends_on:
      - mongo
    networks:
      - reindex

  profile:
    build:
      context: ./reindex/model
      dockerfile: Dockerfile-dev
    working_dir: /usr/src/app
    container_name: ${PROJECT_NAME}_profile
    volumes:
      - ./reindex/model/src:/usr/src/app/src
      - "./modules/plugins:/usr/src/app/node_modules/@reindex/plugins"
      - "./modules/plugins:/usr/src/app/plugins"
    environment:
      MONGO_DB_URI: "mongodb://mongo:${MONGO_PORT}/${MONGO_DB_NAME}"
      MONGO_LOST_CONNECTION_RETRIES_ALERT: 5
      ELASTIC_SEARCH_URL: "elasticsearch:${ELASTICSEARCH_PORT}"
      PORT: "${PROFILE_API_PORT}"
      DEBUG: 3
      NODE_ENV: development
      JWT_SECRET: "newstart"
      JWT_EXPIRES_IN: "1d"
      ADMIN_SECRET_TOKEN: "verysecret"
      ALERT_MANAGER_URL: ""
      ALERT_MANAGER_USER: ""
      ALERT_MANAGER_PASSWORD: ""
      DOCKERFILE_BUILD_PROD_SCRIPT: start
      SERVICE_NAME: "profile"
      PASSWORD_HASHING_ROUNDS: 10
    ports:
      - "${PROFILE_API_PORT}:${PROFILE_API_PORT}"
    depends_on:
      - mongo
    networks:
      - reindex
  

  group:
    build:
      context: ./reindex/model
      dockerfile: Dockerfile-dev
    working_dir: /usr/src/app
    container_name: ${PROJECT_NAME}_groups
    volumes:
      - ./reindex/model/src:/usr/src/app/src
      - "./modules/plugins:/usr/src/app/node_modules/@reindex/plugins"
      - "./modules/plugins:/usr/src/app/plugins"
    environment:
      MONGO_DB_URI: "mongodb://mongo:${MONGO_PORT}/${MONGO_DB_NAME}"
      MONGO_LOST_CONNECTION_RETRIES_ALERT: 5
      ELASTIC_SEARCH_URL: "elasticsearch:${ELASTICSEARCH_PORT}"
      PORT: "${GROUP_API_PORT}"
      DEBUG: 3
      NODE_ENV: development
      SESSION_SECRET: ""
      ADMIN_SECRET_TOKEN: "verysecret"
      ALERT_MANAGER_URL: ""
      ALERT_MANAGER_USER: ""
      ALERT_MANAGER_PASSWORD: ""
      DOCKERFILE_BUILD_PROD_SCRIPT: start
      SERVICE_NAME: "group"
    ports:
      - "${GROUP_API_PORT}:${GROUP_API_PORT}"
    depends_on:
      - mongo
    networks:
      - reindex

  mongo:
    image: mongo:latest
    container_name: ${PROJECT_NAME}_mongo
    ports:
      - "${MONGO_PORT}:${MONGO_PORT}"
    volumes:
      - ./data/mongo:/data/db
    networks:
      - reindex

# Mysql+PHP
  mysql:
    image: mysql:5.7
    container_name: ${PROJECT_NAME}_mysql
    volumes:
      - ./data/mysql:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: "${MYSQL_PASSWORD}"
      MYSQL_DATABASE: "${WORDPRESS_DB_NAME}"
      MYSQL_USER: "${MYSQL_USER}"
      MYSQL_PASSWORD: "${MYSQL_PASSWORD}"
    ports:
      - "${MYSQL_PORT}:${MYSQL_PORT}"
    networks:
      - reindex

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    container_name: ${PROJECT_NAME}_phpmyadmin
    ports:
      - "8081:8081"
    links:
      - "mysql:mysql"
    environment:
      MYSQL_ROOT_PASSWORD: "${MYSQL_PASSWORD}"
    networks:
      - reindex


# Volumes and Network Setup
volumes:
  esdata:
  dist:
  wordpress:

networks:
  reindex:
    driver: bridge
    name: "${REINDEX_NETWORK_NAME}"

# Have a notice if working from another docker-compose file make sure you add
# networks:
#   reindex:
#     external: true
#     name: <SAME NETWORK NAME>
#
# and apply that network to the services in that compose so they can interact with this deployment
