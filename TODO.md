# Core build up
Consumer:
- Verify Exported interfaces and adjustments

Graph:
- Verify Exported interfaces and adjustments

Model:
- Rename ..... EntitiesController?
- Verify Exported interfaces and adjustments

UI:
- Adjust react to export asbtraction to register widgets and contexts

# Utils
Dockerfiles:
- Make sure core modules contain development only dockerfiles

Devops?

Wordpress location? unsure

Docker-compose:
- adjustments to use reindex/core

Integration Repository:
- remove any submodule not in reindex folder
- contrib/base shouldnt be a repository
- Move to gitlab.com/linnovate/reindex V
